import * as React from "react";
import { useEffect, useState } from "react";
import {
  ActivityIndicator,
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { MaterialIcons, FontAwesome } from "@expo/vector-icons";
import { LogBox } from 'react-native';

LogBox.ignoreLogs(['VirtualizedLists should never be nested inside plain ScrollViews with the same orientation - use another VirtualizedList-backed container instead.']);
import { Data } from "../types";


const RecordsURL = "https://testffc.nimapinfotech.com/testdata.json";

const Records = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState<Data[]>([]);
  // const [Status, setStatus] = useState([]);

  // const [description, setDescription] = useState([]
  useEffect(() => {
    fetch(RecordsURL)
      .then((response) => response.json())
      .then((json) => {
        setData(json.data.Records);
        // setStatus(json.Status);
      })
      .catch((error) => alert(error))
      .finally(() => setLoading(false));
  });

  return (
    <SafeAreaView style={styles.container}>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <ScrollView>
          <View>
            <FlatList
              data={data}
              keyExtractor={item => item.Id.toString()}
              renderItem={({ item }) => (
                <View style={styles.big}>
                  {/* images from the API is not valid ...so used rough image for the presentation*/}
                  <Image
                    source={{
                      uri:
                        "https://images.unsplash.com/flagged/photo-1567002349727-f1d1dcdab101?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NHx8cG92ZXJ0eXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80",
                    }}
                    style={styles.imageView}
                  />
                  <View style={styles.percentcontainer}>
                    <Text
                      style={{
                        color: "#fff",
                        fontWeight: "bold",
                        fontSize: 20,
                      }}
                    >
                      100%
                    </Text>
                  </View>

                  <View style={styles.midcontainer}>
                    <View style={styles.titlecontainer}>
                      <Text style={styles.title}>{item.title}</Text>
                      <MaterialIcons
                        name="favorite"
                        size={24}
                        color="#da0e00"
                      />
                    </View>
                    <Text style={{ fontSize: 16 }}>
                      {item.shortDescription}
                    </Text>
                  </View>
                  <View>
                    <View style={styles.datacontainer}>
                      <View
                        style={{
                          flexDirection: "column",
                          justifyContent: "center",
                        }}
                      >
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                            marginBottom: 15,
                          }}
                        >
                          <FontAwesome name="rupee" size={16} color="#fff" />
                          <Text style={styles.data}>{item.collectedValue}</Text>
                        </View>
                        <Text style={styles.label}>FUNDED</Text>
                      </View>
                      <View
                        style={{
                          flexDirection: "column",
                          justifyContent: "center",
                        }}
                      >
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "center",
                            alignItems: "center",
                            marginBottom: 15,
                          }}
                        >
                          <FontAwesome name="rupee" size={16} color="#fff" />
                          <Text style={styles.data}>{item.totalValue}</Text>
                        </View>
                        <Text style={styles.label}>GOALS</Text>
                      </View>
                      <View
                        style={{
                          flexDirection: "column",
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                      >
                        <View
                          style={{
                            marginBottom: 15,
                          }}
                        >
                          <Text style={styles.data}>{item.endDate}</Text>
                        </View>
                        <Text style={styles.label}>ENDS IN</Text>
                      </View>

                      <TouchableOpacity
                        style={[
                          styles.buttonLargeContainer,
                          styles.primaryButton,
                        ]}
                        onPress={() => {}}
                      >
                        <Text style={styles.buttonText}>PLEDGE</Text>
                      </TouchableOpacity>
                    </View>
                    <View style={styles.labelcontainer}></View>
                  </View>
                </View>
              )}
            />
          </View>
        </ScrollView>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  percentcontainer: {
    height: 80,
    width: 80,
    marginLeft: 302,
    top: -40,
    flexDirection: "row",
    backgroundColor: "#156587",
    borderRadius: 40,
    padding: 15,
    justifyContent: "center",
    alignItems: "center",
    zIndex: 100,
  },

  big: {
    backgroundColor: "#1b84b1",
    height: 420,
  },

  midcontainer: {
    padding: 10,
    margin: 10,
    borderRadius: 10,
    height: 120,
    width: "72%",
    backgroundColor: "#fff",
    paddingRight: 20,
    top: -150,
  },

  titlecontainer: {
    zIndex: 100,
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 30,
  },

  title: {
    fontWeight: "bold",
    fontSize: 18,
    color: "#000",
  },

  imageView: {
    height: 250,
    width: 500,
    justifyContent: "center",
    alignItems: "center",
    resizeMode: "cover",
    backgroundColor: "grey",
  },

  data: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 16,
  },

  datacontainer: {
    justifyContent: "space-around",
    flexDirection: "row",
    top: -138,
    paddingLeft: 5,
  },

  label: {
    color: "#fff",
    fontSize: 15,
    fontWeight: "bold",
  },

  labelcontainer: {
    justifyContent: "space-around",
    flexDirection: "row",
    top: -138,
    paddingLeft: 5,
    paddingRight: 160,
  },

  description: {
    fontSize: 16,
  },

  buttonLargeContainer: {
    height: 40,
    width: 120,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    marginRight: 5,
    borderRadius: 16,
  },
  primaryButton: {
    backgroundColor: "#FFF",
  },
  buttonText: {
    color: "#1b84b1",
    fontSize: 18,
    fontWeight: "bold",
  },

  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
});

export default Records;
